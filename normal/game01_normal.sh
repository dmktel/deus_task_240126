#!/bin/sh

# update and upgrade Ubuntu
sudo apt-get update && sudo apt upgrade -y

# Install git
sudo apt-get install -y git curl wget

# clone project
git clone https://gitfront.io/r/deusops/JnacRhR4iD8q/2048-game.git

# create Dockerfile
touch /home/vagrant/2048-game
# set up Dockerfile
sudo bash -c cat > /home/vagrant/2048-game/Dockerfile << "EOF"
# Stage 1: Build Stage
FROM node:16-alpine AS build
WORKDIR /app
# Copy package.json and package-lock.json
COPY package*.json ./
# Install dependencies
RUN npm install --include=dev

# Copy source code
COPY . .

# Build the application
RUN npm run build

# Stage 2: Production Stage
FROM nginx:stable-alpine

# Copy built files from the build stage to the production image
COPY --from=build /app/dist /usr/share/nginx/html
EXPOSE 80
# Container startup command for the web server (nginx in this case)
CMD ["nginx", "-g", "daemon off;"]
EOF

# go to project directory
cd /home/vagrant/2048-game

# INSTALL DOCKER
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

# To install the latest version
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Create the docker group
#sudo groupadd docker

# Add your user to the docker group
sudo usermod -aG docker vagrant

# Activate the changes to groups
sudo newgrp docker





