# deus_task_240126

#### LIGHT

1. Deploy virtual machines with linux: game01.  You can use any creation method, for example in Yandex cloud or locally on Vagrant
2. Install and configure nginx on virtual machine 'game01'. Configure the server as a proxy on localhost
3. Deploy the Game (https://gitfront.io/r/deusops/JnacRhR4iD8q/2048-game/) on the virtual machine and configure nginx to open the site with the game
4. Create a linux system unit responsible for running the service with the game.
5. Make the simplest automation for server configuration: bash scripts or vagrant provisioning.

------

**NORMAL**

1. Write a Dockerfile for the game (project uses node16), need to make the image lightweight via multistaging.
2. Install Docker on "game01" and run the game in a container without external nginx
3. Write a gitlab-ci pipline to automatically build and publish Docker image c application.
4. Write ansible 'docker' role to install docker. The role should pass molecule tests.
5. Write ansible-playbook to configure the server 'game01' at Light-5 level and a system unit template to start the docker container with the application we need.
6. Add to guitlab-ci a step with the deployment of the application to the server. Ansible roles should be in separate repositories and work through ansible-galaxy.
7. Increase the number of servers to three and configure distribution of incoming requests via Haproxy.

------

**Prerequisites:**

1. Vagrant v2.4 installed
2. Vagrant provider: virtualbox
3. Vagrant box 'bento/ubuntu-22.04'

------

